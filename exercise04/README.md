This repo contains selected exercises to K. N. Kings C Programming: A Modern Approach.

a) What is the output when testdebug.c is executed?
Output if DEBUG is defined:
Value of i: 1
Value of j: 2
Value of k: 3
Value of i + j: 3
Value of 2 * i + j - k: 1

b) What is the output if the #define directive is removed from testdebug.c?
Output if DEBUG is not defined:

c) Explain why the output is different in parts (a) and (b)
Defining the DEBUG macro has an effect on how the parameterized macro 
PRINT_DEBUG(n) is defined, thus influencing the output of the program.

d) Is it necessary for the DEBUG macro to be defined before debug.h is included in order for PRINT_DEBUG to have the desired effect? Justify your answer.
Yes. If DEBUG is defined after debug.h is included then the definition of PRINT_DEBUG(n) is 
#define PRINT_DEBUG(n)
so the effect is the same as if DEBUG had not been defined at all.
